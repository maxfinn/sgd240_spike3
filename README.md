﻿# Spike Report

## Blueprint Basics

### Introduction

In Unreal Engine, a lot of work is completed using Blueprints, a form of Visual Programming/Scripting.
We have not previously been exposed to such a system.

### Goals

1. In a new Blueprint FPS Unreal Engine project, create a “Launch-pad” actor, which can be placed to cause the player to leap into the air upon stepping on it.
    1. Play a sound when the character is launched
    1. Set the Launch Velocity using a variable, which lets you place multiple launch-pads with different velocities in the same level
    1. Add an Arrow Component, and use its rotation to define the direction to launch the character
1. Create a new level using the default elements available to you from the Unreal Engine (boxes should suffice), which should be a fun little FPS level with many launch-pads that help you access different areas.


### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Blueprints Quick Start Guide](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/QuickStart/index.html)
* [UE4 AnswerHub - Playing a sound from Blueprint](https://answers.unrealengine.com/questions/324950/playing-a-sound-from-blueprint.html)
* [FreeSound.org](http://freesound.org/)

### Tasks Undertaken

1. Create a Blueprint called LaunchPad, adding Cube, Box and Arrow components to it
1. Resize the Cube to something appropriate for a launch pad, then resize the Box to have smaller X and Y values but a bigger Z value
1. In the event graph, set up nodes as described in the Quick Start Guide
![LaunchPad blueprint as described in Quick Start Guide][LaunchPad01]
1. Extend this functionality as shown below, using `GetForwardVector` with the Arrow component for launch angle, a Vector * Float multiplication with a user created LaunchVelocity variable for launch force control, and `Play` with an Audio component to play a matching sound
![LaunchPad blueprint with additional nodes added to complete functional requirements][LaunchPad02]
1. Place several of these LaunchPad actors into the scene, rearranging the layout as required to make a simple level design
1. Adjust each LaunchPad's publicly exposed LaunchVelocity variable to launch the character as needed

### What we found out

Having been out of practice with UE4, I had forgotten some basic editor usage skills, namely how to interact with the subcomponents of a given actor inside the main editor (as opposed to the Blueprint editor). My Google search terms yielded little in the way of a solution, and ultimately it came as an accident that I glanced upon the actor hierarchy in the top half of the Details panel, which had been mostly squashed from view as a result of expanding the section below it. Not knowing the method of adjustment for some time prior to that realisation, I opted to set up the Construction Script with a second variable to allow for the Arrow's angle modification in the same way as the LaunchVelocity.

![GIF showing the launch angle being adjusted by variable in the editor][LaunchPad03]

### Open Issues/Risks

1. At current, adding a new audio asset involved placing the .wav file into the project folder and referencing it, which created a corresponding .uasset file in the same location. The pre-existing FPS audio file does not have a .wav file (or other audio source file) in the same location, however attempts made to remove the LaunchPad's original audio file broke the asset in game, requiring it be returned to the project folder.
    * I suspect that there is either a setting to properly insert the audio data into the .uasset file, allowing removal of the original source, or a general practice of creating a separate folder elsewhere for actual source files, allowing them to be hidden from the regular working folders.


[LaunchPad01]: https://monosnap.com/file/ktonXsu5XbPZPG9OtRL7rovSlYgIoS.png "Initial LaunchPad setup"
[LaunchPad02]: https://monosnap.com/file/sHyQkqo1YSQGt6rzTqlNa4iA6jVMzb.png "Extended functionality"
[LaunchPad03]: https://monosnap.com/file/yEzdCDbRiaxaA00qks9AA4ajRcQTG7.png "Adjusting the angle of launch in editor"